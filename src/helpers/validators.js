export function courseValidator({ title, description, authors, duration }) {
	const titleValidation = nullStringValidator(title);
	const descriptionValidation = descriptionValidator(description);
	const authorsValidation = authorsValidator(authors);
	const durationValidation = durationValidator(duration);
	const allValid =
		titleValidation.isValid &&
		descriptionValidation.isValid &&
		authorsValidation.isValid &&
		durationValidation.isValid;

	return {
		titleValidation,
		descriptionValidation,
		authorsValidation,
		durationValidation,
		allValid,
	};
}

export function authorValidator({ name }) {
	const nameValidation = nullStringValidator(name);
	const allValid = nameValidation.isValid;

	return {
		nameValidation,
		allValid,
	};
}

//helpers
function nullStringValidator(string) {
	return string
		? { isValid: true, message: '' }
		: { isValid: false, message: "can't be empty" };
}

function authorsValidator(authors) {
	if (authors && authors.length === 0) {
		return { isValid: false, message: 'you must select at least one author' };
	}
	return { isValid: true, message: '' };
}

function descriptionValidator(description) {
	if (description.length <= 2) {
		return { isValid: false, message: 'you must select at least one author' };
	}
	return { isValid: true, message: '' };
}

function durationValidator(duration) {
	if (duration < 1) {
		return { isValid: false, message: 'you must select at least one author' };
	}
	return { isValid: true, message: '' };
}
