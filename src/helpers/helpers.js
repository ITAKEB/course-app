export function toHoursAndMinutes(totalMinutes) {
	const hours = Math.floor(totalMinutes / 60);
	const minutes = totalMinutes % 60;

	return `${hours}:${minutes}`;
}

export function toDate(date) {
	const validDate = date.replaceAll('/', '.');

	return validDate;
}

export function getAuthorsName(authors, authorsNameById) {
	const authorsName = authors.map((authorId) => authorsNameById[authorId]);

	return authorsName;
}

export function saveUserSession(jwt, user) {
	localStorage.setItem('jwt', jwt);

	const { name, email, role } = user;

	if (name) {
		localStorage.setItem('name', name);
	}
	localStorage.setItem('role', role);
	localStorage.setItem('email', email);
}

export function getUserSession() {
	const token = localStorage.getItem('jwt');

	let user = {};
	user['name'] = localStorage.getItem('name');
	user['email'] = localStorage.getItem('email');
	user['role'] = localStorage.getItem('role');

	return { token, user };
}

export function clearUserSession() {
	localStorage.removeItem('jwt');

	localStorage.removeItem('name');
	localStorage.removeItem('role');
	localStorage.removeItem('email');
}
