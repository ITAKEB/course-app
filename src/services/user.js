export async function registerUser(userPayload) {
	const host = process.env.REACT_APP_API_HOST;
	const headers = new Headers();

	headers.append('Content-Type', 'application/json');

	const response = await fetch(`${host}/register`, {
		method: 'POST',
		credentials: 'same-origin',
		headers,
		body: JSON.stringify(userPayload),
	});

	const code = response.status;

	if ([200, 201].includes(code)) {
		const { result } = await response.json();

		return { result, isSuccess: true };
	} else {
		const { errors } = await response.json();

		return { errors, isSuccess: false };
	}
}

export async function postUser(userPayload) {
	const host = process.env.REACT_APP_API_HOST;
	const headers = new Headers();

	headers.append('Content-Type', 'application/json');

	const response = await fetch(`${host}/login`, {
		method: 'POST',
		credentials: 'same-origin',
		headers,
		body: JSON.stringify(userPayload),
	});

	const code = response.status;

	if ([200, 201].includes(code)) {
		const { result } = await response.json();

		return { result, isSuccess: true };
	} else {
		const { errors } = await response.json();

		return { errors, isSuccess: false };
	}
}

export async function getUser(jwt) {
	const host = process.env.REACT_APP_API_HOST;
	const headers = new Headers();

	headers.append('Authorization', jwt);

	const response = await fetch(`${host}/users/me`, {
		method: 'GET',
		credentials: 'same-origin',
		headers,
	});

	const code = response.status;

	if ([200, 201].includes(code)) {
		const { result } = await response.json();

		return { result, isSuccess: true };
	} else {
		const { errors } = await response.json();

		return { errors, isSuccess: false };
	}
}

export async function logoutUser(jwt) {
	const host = process.env.REACT_APP_API_HOST;
	const headers = new Headers();

	headers.append('Authorization', jwt);

	const response = await fetch(`${host}/logout`, {
		method: 'DELETE',
		credentials: 'same-origin',
		headers,
	});

	const code = response.status;

	if ([200, 201].includes(code)) {
		return { isSuccess: true };
	} else {
		const { errors } = await response.json();

		return { errors, isSuccess: false };
	}
}

export function mkRegisterUserPayload(name, password, email) {
	return { name, password, email };
}

export function mkPostUserPayload(email, password) {
	return { email, password };
}
