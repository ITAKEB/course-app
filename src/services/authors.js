export async function postAuthor(authorPayload, jwt) {
	const host = process.env.REACT_APP_API_HOST;
	const headers = new Headers();

	headers.append('Content-Type', 'application/json');
	headers.append('Authorization', jwt);

	const response = await fetch(`${host}/authors/add`, {
		method: 'POST',
		credentials: 'same-origin',
		headers,
		body: JSON.stringify(authorPayload),
	});

	const code = response.status;

	if ([200, 201].includes(code)) {
		const { result } = await response.json();

		return { result, isSuccess: true };
	} else {
		const { errors } = await response.json();

		return { errors, isSuccess: false };
	}
}

export async function getAllAuthors() {
	const host = process.env.REACT_APP_API_HOST;

	const response = await fetch(`${host}/authors/all`, {
		method: 'GET',
	});

	const code = response.status;

	if ([200, 201].includes(code)) {
		const { result } = await response.json();

		return { result, isSuccess: true };
	} else {
		const { errors } = await response.json();

		return { errors, isSuccess: false };
	}
}

export async function getAuthorById(id) {
	const host = process.env.REACT_APP_API_HOST;

	const response = await fetch(`${host}/authors/${id}`, {
		method: 'GET',
	});

	const code = response.status;

	if ([200, 201].includes(code)) {
		const { result } = await response.json();

		return { result, isSuccess: true };
	} else {
		const { errors } = await response.json();

		return { errors, isSuccess: false };
	}
}

export async function deleteAuthorById(jwt, id) {
	const host = process.env.REACT_APP_API_HOST;
	const headers = new Headers();

	headers.append('Authorization', jwt);

	const response = await fetch(`${host}/authors/${id}`, {
		method: 'DELETE',
		credentials: 'same-origin',
		headers,
	});

	const code = response.status;

	if ([200, 201].includes(code)) {
		return { isSuccess: true };
	} else {
		const { errors } = await response.json();

		return { errors, isSuccess: false };
	}
}

export function mkPostAuthorPayload({ name }) {
	return { name };
}
