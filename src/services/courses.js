export async function postCourse(coursePayload, jwt) {
	const host = process.env.REACT_APP_API_HOST;
	const headers = new Headers();

	headers.append('Content-Type', 'application/json');
	headers.append('Authorization', jwt);

	const response = await fetch(`${host}/courses/add`, {
		method: 'POST',
		credentials: 'same-origin',
		headers,
		body: JSON.stringify(coursePayload),
	});

	const code = response.status;

	if ([200, 201].includes(code)) {
		const { result } = await response.json();

		return { result, isSuccess: true };
	} else {
		const { errors } = await response.json();

		return { errors, isSuccess: false };
	}
}

export async function getAllCourses() {
	const host = process.env.REACT_APP_API_HOST;

	const response = await fetch(`${host}/courses/all`, {
		method: 'GET',
	});

	const code = response.status;

	if ([200, 201].includes(code)) {
		const { result } = await response.json();

		return { result, isSuccess: true };
	} else {
		const { errors } = await response.json();

		return { errors, isSuccess: false };
	}
}

export async function getCourseById(id) {
	const host = process.env.REACT_APP_API_HOST;

	const response = await fetch(`${host}/courses/${id}`, {
		method: 'GET',
	});

	const code = response.status;

	if ([200, 201].includes(code)) {
		const { result } = await response.json();

		return { result, isSuccess: true };
	} else {
		const { errors } = await response.json();

		return { errors, isSuccess: false };
	}
}

export async function updateCourseById(coursePayload, jwt, id) {
	const host = process.env.REACT_APP_API_HOST;
	const headers = new Headers();

	headers.append('Content-Type', 'application/json');
	headers.append('Authorization', jwt);

	const response = await fetch(`${host}/courses/${id}`, {
		method: 'PUT',
		credentials: 'same-origin',
		headers,
		body: JSON.stringify(coursePayload),
	});

	const code = response.status;

	if ([200, 201].includes(code)) {
		const { result } = await response.json();

		return { result, isSuccess: true };
	} else {
		const { errors } = await response.json();

		return { errors, isSuccess: false };
	}
}

export async function deleteCourseById(jwt, id) {
	const host = process.env.REACT_APP_API_HOST;
	const headers = new Headers();

	headers.append('Authorization', jwt);

	const response = await fetch(`${host}/courses/${id}`, {
		method: 'DELETE',
		credentials: 'same-origin',
		headers,
	});

	const code = response.status;

	if ([200, 201].includes(code)) {
		return { isSuccess: true };
	} else {
		const { errors } = await response.json();

		return { errors, isSuccess: false };
	}
}

export function mkPostCoursePayload({ title, description, duration, authors }) {
	return { title, description, duration: Number(duration), authors };
}

export function mkPutCoursePayload({ title, description, duration, authors }) {
	return { title, description, duration: Number(duration), authors };
}
