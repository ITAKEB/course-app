import React from 'react';
import './App.css';
import CurrentComponent from './components/CurrentComponent/CurrentComponent';

function App() {
	return (
		<div>
			<CurrentComponent />
		</div>
	);
}
export default App;
