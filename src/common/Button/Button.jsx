import { Button as MaterialButton } from '@mui/material';
import React from 'react';

const styles = {
	buttonBase: {
		mt: 1,
		':hover': {
			fontWeight: 'bold',
			transform: 'scale(1.05)',
		},
		'&.MuiButtonBase-root': {
			height: 'min-content',
			margin: 0,
		},
		'&.MuiButtonBase-root p': {
			margin: 0,
		},
	},
};

function Button({
	children,
	onClick,
	type = 'button',
	color = 'primary',
	variant = 'contained',
	sx = {},
}) {
	return (
		<MaterialButton
			sx={{ ...styles.buttonBase, ...sx }}
			variant={variant}
			type={type}
			onClick={onClick}
			color={color}
		>
			{children}
		</MaterialButton>
	);
}

export default Button;
