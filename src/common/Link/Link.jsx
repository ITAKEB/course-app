import React from 'react';
import { Link as RouterLink } from 'react-router-dom';

const styles = {
	rourterLink: { textDecoration: 'none' },
};

function Link({ to, sx = {}, children, reloadDocument = false }) {
	return (
		<RouterLink
			reloadDocument={reloadDocument}
			to={to}
			style={{ ...styles.rourterLink, ...sx }}
		>
			{children}
		</RouterLink>
	);
}

export default Link;
