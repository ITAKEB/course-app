import { TextField } from '@mui/material';
import React from 'react';

const styles = {
	textField: {
		width: '100%',
	},
};

function Input({
	labelText,
	placeholderText,
	onChange,
	required = false,
	value = undefined,
	type = 'text',
	variant = 'outlined',
}) {
	return (
		<>
			<TextField
				id={labelText}
				sx={styles.textField}
				label={labelText}
				required={required}
				variant={variant}
				onChange={onChange}
				value={value}
				type={type}
				placeholder={placeholderText}
			/>
		</>
	);
}

export default Input;
