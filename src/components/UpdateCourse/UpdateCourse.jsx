import React, { useEffect, useState } from 'react';
import CourseForm from '../CourseForm/CourseForm';
import { authorValidator, courseValidator } from '../../helpers/validators';
import { asyncActionUpdateCourse } from '../../store/courses/thunk';
import {
	asyncActionGetAllAuthors,
	asyncActionSaveAuthor,
} from '../../store/authors/thunk';
import { getUserSession } from '../../helpers/helpers';
import { getCourseById, mkPutCoursePayload } from '../../services/courses';
import { mkPostAuthorPayload } from '../../services/authors';
import { useNavigate, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

function UpdateCourse() {
	const { courseId } = useParams();

	const dispatch = useDispatch();
	const authorsSelector = useSelector((state) => state.authors);

	const navigate = useNavigate();

	const authorsNameById = {};
	authorsSelector.forEach(
		(author) => (authorsNameById[author.id] = author.name)
	);
	const [courseInfo, setCourseInfo] = useState({
		title: '',
		description: '',
		currentAuthorsList: [],
		authorsSelected: [],
		authorsIds: [],
		duration: 0,
	});
	const [author, setAuthor] = useState({
		name: '',
	});

	/*
  I only thought this way to control the state of this component.

  Maybe it's not the most efficient way to do this, but I think the main reason is that the
  backend only returns the id's of the course authors, so I have to ask bak-end one by one
  to get them. 
  
  Another reason is that the backend response is a json and the id of each author
  inside that json is not the author id, each author id is inside the author object, so I think
  it would be faster if the json response mapped the authors by author id.

  If there is a more efficient way to do this, please let me know, I want to learn :)
  */
	useEffect(() => {
		console.log('useEffect');
		//If authorsSelector is empty, so get all from bakend
		if (authorsSelector.length < 1) {
			dispatch(asyncActionGetAllAuthors());
			return;
		}

		const fetchData = async () => {
			const fetchCourse = await getCourseById(courseId);
			if (!fetchCourse.isSuccess) {
				console.log(fetchCourse.errors);
				return;
			}
			const course = fetchCourse.result;
			const courseAuthorsIds = course.authors;

			//First way - Getting each user for backend
			// let authorsSelected = await Promise.all(
			// 	courseAuthorsIds.map(async (authorId) => {
			// 		const author = await getAuthorById(authorId);
			// 		if (author.isSuccess) return author.result;
			// 		console.log('User not found');
			// 	})
			// );

			//Second way - Getting each user for store
			let authorsSelected = courseAuthorsIds.map((authorId) => {
				const author = authorsSelector.find((author) => author.id === authorId);
				return author;
			});

			const updateState = (prevSt) => {
				let currentAuthorsList = prevSt.currentAuthorsList;
				let currentCourseAuthorsIds = prevSt.authorsIds;
				let currentAuthorsSelected = prevSt.authorsSelected;

				courseAuthorsIds.forEach((authorId) => {
					if (!currentCourseAuthorsIds.includes(authorId)) {
						currentCourseAuthorsIds.push(authorId);
					}
				});

				authorsSelector.forEach((fetchAuthor) => {
					// console.log(fetchAuthor);
					if (
						!currentAuthorsList.find(
							(author) => author.id === fetchAuthor.id
						) &&
						!currentCourseAuthorsIds.includes(fetchAuthor.id)
					) {
						currentAuthorsList.push(fetchAuthor);
					}
				});

				if (currentAuthorsSelected.length < 1) {
					authorsSelected.forEach((author) => {
						if (
							!currentAuthorsSelected.find(
								(authorSelected) => authorSelected.id === author.id
							)
						) {
							currentAuthorsSelected.push(author);
						}
					});
				}

				return {
					...prevSt,
					title: course.title,
					description: course.description,
					duration: course.duration,
					authorsIds: currentCourseAuthorsIds,
					currentAuthorsList,
					authorsSelected: currentAuthorsSelected,
				};
			};

			setCourseInfo(updateState);
		};

		fetchData();
	}, [authorsSelector, courseId, dispatch]);

	function onChangeCourseInfo(attr) {
		return (evt) => {
			evt.preventDefault();
			setCourseInfo((prevSt) => ({ ...prevSt, [attr]: evt.target.value }));
		};
	}

	function onChangeAuthorName(evt) {
		evt.preventDefault();
		setAuthor((prevSt) => ({ ...prevSt, name: evt.target.value }));
	}

	function handleAddAuthor(id, name) {
		return (evt) => {
			evt.preventDefault();
			let newAuthors = courseInfo.authorsSelected;
			let newAuthorsIds = courseInfo.authorsIds;

			let newcurrentAuthorsList = courseInfo.currentAuthorsList.filter(
				(author) => author.id !== id
			);
			setCourseInfo((prevSt) => ({
				...prevSt,
				currentAuthorsList: newcurrentAuthorsList,
			}));

			newAuthors.push({ name, id });
			newAuthorsIds.push(id);
			setCourseInfo((prevSt) => ({
				...prevSt,
				authorsSelected: newAuthors,
				authorsIds: newAuthorsIds,
			}));
		};
	}

	function handleDeleteAuthor(id) {
		return (evt) => {
			evt.preventDefault();
			let authorDeleted = null;
			let newAuthors = courseInfo.authorsSelected.filter((author) => {
				if (author.id !== id) {
					return true;
				} else {
					authorDeleted = author;
					return false;
				}
			});
			let newAuthorsId = courseInfo.authorsIds.filter(
				(authorId) => authorId !== id
			);

			setCourseInfo((prevSt) => ({
				...prevSt,
				authorsSelected: newAuthors,
				authorsIds: newAuthorsId,
			}));

			let newcurrentAuthorsList = courseInfo.currentAuthorsList;
			newcurrentAuthorsList.push(authorDeleted);
			setCourseInfo((prevSt) => ({
				...prevSt,
				currentAuthorsList: newcurrentAuthorsList,
			}));
		};
	}

	function onSubmitAuthor(evt) {
		evt.preventDefault();
		const validator = authorValidator(author);
		if (!validator.allValid) {
			window.alert('Please, add an author name');
			return;
		}
		const newAuthor = {
			name: author.name,
		};
		const session = getUserSession();
		const authorPayload = mkPostAuthorPayload(newAuthor);

		dispatch(asyncActionSaveAuthor(authorPayload, session.token));

		setAuthor((prevSt) => ({ ...prevSt, name: '' }));
	}

	function onUpdateCourse(evt) {
		evt.preventDefault();
		const validator = courseValidator(courseInfo);
		if (!validator.allValid) {
			window.alert('Please, fill in all fields');
			return;
		}

		const courseUpdated = {
			title: courseInfo.title,
			description: courseInfo.description,
			duration: courseInfo.duration,
			authors: courseInfo.authorsIds,
		};
		const session = getUserSession();

		const coursePayload = mkPutCoursePayload(courseUpdated);

		dispatch(asyncActionUpdateCourse(coursePayload, courseId, session.token));

		navigate('/courses');
		console.log('Sucess Created');
	}

	return (
		<CourseForm
			onSubmitCourse={onUpdateCourse}
			onSubmitAuthor={onSubmitAuthor}
			handleDeleteAuthor={handleDeleteAuthor}
			onChangeAuthorName={onChangeAuthorName}
			handleAddAuthor={handleAddAuthor}
			onChangeCourseInfo={onChangeCourseInfo}
			courseInfo={courseInfo}
			author={author}
			type='update'
		/>
	);
}

export default UpdateCourse;
