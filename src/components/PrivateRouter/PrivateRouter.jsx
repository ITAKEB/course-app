import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Navigate } from 'react-router-dom';
import { actionSaveUser } from '../../store/user/actionCreators';
import { getUserSession } from '../../helpers/helpers';

function PrivateRouter({ children }) {
	const userSelector = useSelector((state) => state.user);
	const session = getUserSession();

	const dispatch = useDispatch();

	useEffect(() => {
		if (!userSelector) {
			if (session.token) {
				dispatch(actionSaveUser(session.user, session.token));
			}
		}
	});

	const renderRoute =
		session && session.user.role === 'admin' ? (
			children
		) : (
			<Navigate to='/courses' />
		);

	return renderRoute;
}

export default PrivateRouter;
