import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { Card, Button, Grid, Typography } from '@mui/material';
import KeyboardBackspaceIcon from '@mui/icons-material/KeyboardBackspace';

import Link from '../../common/Link/Link';
import { toDate, toHoursAndMinutes } from '../../helpers/helpers';
import { useDispatch, useSelector } from 'react-redux';
import { getCourseById } from '../../services/courses';
import { asyncActionGetAllAuthors } from '../../store/authors/thunk';

const styles = {
	backButton: { margin: '20px' },
	longTextWrap: {
		overflowWrap: 'anywhere',
	},
};

function CourseInfo() {
	const { courseId } = useParams();
	const coursesSelector = useSelector((state) => state.courses);
	const authorsSelector = useSelector((state) => state.authors);
	const dispatch = useDispatch();

	const [state, setState] = useState({
		id: '',
		title: '',
		authors: [],
		duration: '',
		createdAt: '',
		description: '',
	});

	useEffect(() => {
		//If authorsSelector is empty, so get all from bakend
		if (authorsSelector.length < 1) {
			dispatch(asyncActionGetAllAuthors());
			return;
		}

		const authorsNameById = {};
		authorsSelector.forEach(
			(author) => (authorsNameById[author.id] = author.name)
		);

		const fetchData = async () => {
			let course = coursesSelector.find((course) => course.id === courseId);

			//If course is not in the store, so ask bakend for it
			if (!course) {
				const fetchCourse = await getCourseById(courseId);
				if (fetchCourse.isSuccess) {
					course = fetchCourse.result;
				} else {
					console.log(fetchCourse.errors);
					return;
				}
			}

			const updateState = (prevSt) => {
				let newState = { ...prevSt };

				const authors = course.authors
					? course.authors.map((authorId) => authorsNameById[authorId])
					: [];
				const createdAt = toDate(course.creationDate);
				const duration = toHoursAndMinutes(course.duration);
				newState = {
					...course,
					authors: authors.toString(),
					duration,
					createdAt,
				};
				return newState;
			};

			setState(updateState);
		};

		fetchData();
	}, [courseId, authorsSelector, coursesSelector, dispatch]);

	return (
		<div>
			<Card>
				<div style={styles.backButton}>
					<Link to='/courses'>
						<Button variant='text' startIcon={<KeyboardBackspaceIcon />}>
							Back to courses
						</Button>
					</Link>
				</div>
				<Grid container direction='row' alignContent='center' p={4}>
					<Grid
						item
						xs={12}
						md={12}
						sm={12}
						mb={5}
						alignContent='center'
						justifyContent='center'
					>
						<Typography align='center' variant='h4'>
							{state.title}
						</Typography>
					</Grid>
					<Grid item container direction='row' spacing={5}>
						<Grid item xs={12} md={8} sm={8}>
							<Typography>{state.description}</Typography>
						</Grid>
						<Grid item container direction='column' xs={12} md={4} sm>
							<Grid item sx={{ ...styles.longTextWrap }}>
								<Typography>
									<b>ID:</b> {state.id}
								</Typography>
							</Grid>
							<Grid item>
								<Typography>
									<b>Duration:</b> {state.duration} hours
								</Typography>
							</Grid>
							<Grid item>
								<Typography>
									<b>Created:</b> {state.createdAt}
								</Typography>
							</Grid>
							<Grid item sx={{ ...styles.longTextWrap }}>
								<Typography paragraph>
									<b>Authors:</b> {state.authors}
								</Typography>
							</Grid>
							<Grid item>
								<Typography></Typography>
							</Grid>
						</Grid>
					</Grid>
				</Grid>
			</Card>
		</div>
	);
}

export default CourseInfo;
