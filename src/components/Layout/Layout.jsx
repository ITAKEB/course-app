import React, { useEffect } from 'react';
import { Outlet, useLocation, useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import Header from '../Header/Header';
import { getUserSession } from '../../helpers/helpers';
import {
	asyncActionDeleteSession,
	asyncActionSaveSession,
} from '../../store/user/thunk';

function Layout() {
	const dispatch = useDispatch();

	const location = useLocation();
	const navigate = useNavigate();

	useEffect(() => {
		const session = getUserSession();
		if (session && session.token) {
			const { token } = session;
			dispatch(asyncActionSaveSession(token));

			if (location.pathname === '/') {
				navigate('/courses');
			}
		} else {
			if (location.pathname === '/') {
				navigate('/login');
			}
		}
	}, [location, navigate, dispatch]);

	const handlerLogOut = () => {
		const session = getUserSession();

		dispatch(asyncActionDeleteSession(session.token));
	};

	return (
		<div>
			<Header handlerLogOut={handlerLogOut} />
			<div>
				<Outlet />
			</div>
		</div>
	);
}

export default Layout;
