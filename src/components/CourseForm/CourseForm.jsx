import React from 'react';

import Input from '../../common/Input/Input';
import Button from '../../common/Button/Button';
import Authors from './components/Authors/Authors';
import CourseAuthors from './components/CourseAuthors/CourseAuthors';
import { toHoursAndMinutes } from '../../helpers/helpers';
import { Grid, TextField, Typography } from '@mui/material';

const styles = {
	createCourse_form_descriptionInput: {
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'flex-start',
		width: '100%',
	},
	createCourse_createAuthor_button: {
		maxWidth: '200px',
	},
	createCourse_duration_span: {
		fontWeight: 'bold',
		fontSize: '32px',
	},
};

function CourseForm({
	courseInfo,
	handleDeleteAuthor,
	handleAddAuthor,
	onChangeCourseInfo,
	onSubmitCourse,
	onSubmitAuthor,
	author,
	onChangeAuthorName,
	type = 'create',
}) {
	const showCurrentSelectedAuthors =
		courseInfo.authorsSelected.length > 0 ? (
			<CourseAuthors
				handleDeleteAuthor={handleDeleteAuthor}
				courseAuthorsList={courseInfo.authorsSelected}
			/>
		) : (
			<Typography variant='h6' mt={2} align='center'>
				Author list is empty
			</Typography>
		);

	const showcurrentAuthorsList =
		courseInfo.currentAuthorsList.length > 0 ? (
			<Authors
				authorsList={courseInfo.currentAuthorsList}
				handleAddAuthor={handleAddAuthor}
			/>
		) : (
			<Typography variant='h6' mt={2} align='center'>
				Author list is empty
			</Typography>
		);

	const submitButtonType =
		type === 'create' ? (
			<Button color='success' onClick={onSubmitCourse}>
				Create Course
			</Button>
		) : (
			<Button color='success' onClick={onSubmitCourse}>
				Update Course
			</Button>
		);

	return (
		<>
			<Grid
				data-testid='course-form-container'
				id='course-form-container'
				container
				direction='column'
				spacing={2}
			>
				<Grid item container direction='column' spacing={2}>
					<Grid
						item
						container
						direction='row'
						alignItems='center'
						spacing={2}
						justifyContent='space-between'
					>
						<Grid item>
							<Input
								placeholderText='Enter title'
								labelText='Title'
								value={courseInfo.title}
								onChange={onChangeCourseInfo('title')}
							/>
						</Grid>
						<Grid item>{submitButtonType}</Grid>
					</Grid>
					<Grid item>
						<TextField
							id='description'
							label='Description'
							//multiline transform TextField into text-area
							multiline
							rows={5}
							value={courseInfo.description}
							fullWidth
							onChange={onChangeCourseInfo('description')}
						/>
					</Grid>
				</Grid>
				<Grid item container direction='column' spacing={2}>
					<Grid
						item
						container
						direction='row'
						xs={12}
						md={6}
						sm={6}
						spacing={5}
					>
						<Grid item container direction='column' xs={12} md={6} sm={6}>
							<Grid item>
								<Typography variant='h6' align='center'>
									Add author
								</Typography>
							</Grid>
							<Grid item>
								<form
									style={styles.createCourse_form_descriptionInput}
									id='authorForm'
									onSubmit={onSubmitAuthor}
								>
									<Input
										placeholderText='Enter author name'
										labelText='Author name'
										value={author.name}
										onChange={onChangeAuthorName}
									/>
									<br />
									<Button
										sx={styles.createCourse_createAuthor_button}
										type='submit'
									>
										Create Author
									</Button>
								</form>
							</Grid>
						</Grid>
						<Grid item container direction='column' xs={12} md={6} sm={6}>
							<Grid item>
								<Typography variant='h6' align='center'>
									Authors
								</Typography>
							</Grid>
							<Grid item>{showcurrentAuthorsList}</Grid>
						</Grid>
					</Grid>
					<Grid item container direction='row' spacing={5}>
						<Grid item container direction='column' xs={12} md={6} sm={6}>
							<Grid item>
								<Typography variant='h6' align='center'>
									Duration
								</Typography>
							</Grid>
							<Grid item>
								<Input
									placeholderText='Enter in minutes'
									type='number'
									labelText='Duration'
									value={courseInfo.duration}
									onChange={onChangeCourseInfo('duration')}
								/>
							</Grid>
							<Grid item>
								<Typography variant='body' paragraph>
									Duration:
									<span style={styles.createCourse_duration_span}>
										{toHoursAndMinutes(courseInfo.duration)}
									</span>
									hours
								</Typography>
							</Grid>
						</Grid>
						<Grid item container direction='column' xs={12} md={6} sm={6}>
							<Grid item>
								<Typography variant='h6' align='center'>
									Course Authors
								</Typography>
							</Grid>
							<Grid item>{showCurrentSelectedAuthors}</Grid>
						</Grid>
					</Grid>
				</Grid>
			</Grid>
		</>
	);
}

export default CourseForm;
