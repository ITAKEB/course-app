import React from 'react';
import AuthorCard from '../AuthorCard/AuthorCard';
import { Grid } from '@mui/material';

function Authors({ authorsList, handleAddAuthor }) {
	const currentAuthors = authorsList.map((author) => (
		<AuthorCard
			key={author.id}
			id={author.id}
			name={author.name}
			onClick={handleAddAuthor(author.id, author.name)}
		/>
	));

	return (
		<Grid
			item
			container
			direction='column'
			justifyContent='space-between'
			alignContent='space-between'
		>
			{currentAuthors}
		</Grid>
	);
}

export default Authors;
