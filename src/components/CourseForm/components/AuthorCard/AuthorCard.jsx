import React from 'react';
import Button from '../../../../common/Button/Button';
import { Grid } from '@mui/material';

function AuthorCard({ onClick, type, name, id }) {
	const button =
		type === 'delete' ? (
			<Button color='error' onClick={onClick}>
				Delete author
			</Button>
		) : (
			<Button onClick={onClick}>Add author</Button>
		);

	return (
		<Grid item container direction='row' justifyContent='space-between'>
			<Grid item>
				<p>{name}</p>
			</Grid>
			<Grid item>{button}</Grid>
		</Grid>
	);
}

export default AuthorCard;
