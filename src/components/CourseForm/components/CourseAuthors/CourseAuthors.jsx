import React from 'react';
import AuthorCard from '../AuthorCard/AuthorCard';

function CourseAuthors({ courseAuthorsList, handleDeleteAuthor }) {
	const currentAuthors = courseAuthorsList.map((author) => (
		<AuthorCard
			key={author.id}
			id={author.id}
			name={author.name}
			type='delete'
			onClick={handleDeleteAuthor(author.id)}
		/>
	));

	return <div>{currentAuthors}</div>;
}

export default CourseAuthors;
