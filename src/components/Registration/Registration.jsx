import React, { useState } from 'react';
import { NoteAdd } from '@mui/icons-material';
import {
	Avatar,
	Alert as MaterialAlert,
	Box,
	Container,
	Grid,
	TextField,
	Typography,
} from '@mui/material';

import Button from '../../common/Button/Button';
import Link from '../../common/Link/Link';
import { mkRegisterUserPayload, registerUser } from '../../services/user';
import { useNavigate } from 'react-router-dom';
import Input from '../../common/Input/Input';

const styles = {
	boxContainer: {
		marginTop: 8,
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
	},
	iconForm: {
		m: 1,
		bgcolor: '#AA09EF',
	},
	boxContainerForm: {
		mt: 3,
	},
	logInButtom: {
		mt: 3,
		mb: 2,
		'&.MuiButtonBase-root': {
			marginTop: '25px',
		},
		width: '100%',
	},
};

function Registration() {
	const navigate = useNavigate();
	const [state, setState] = useState({
		name: '',
		email: '',
		password: '',
		errors: null,
		showAlert: false,
	});

	const loginMessage = 'If you not have an account you can ';

	function handleInputChange(attr) {
		return (evt) => {
			evt.preventDefault();
			const value = evt.target.value;
			setState((prevSt) => ({ ...prevSt, [attr]: value }));
		};
	}

	async function handleSubmit(evt) {
		evt.preventDefault();
		const user = mkRegisterUserPayload(state.name, state.password, state.email);
		const response = await registerUser(user);

		if (response.isSuccess) {
			navigate('/login');
		} else {
			const errors = response.errors
				? response.errors
				: 'An error has occurred';

			setState((prevSt) => ({ ...prevSt, errors, showAlert: true }));
			console.log(errors);
		}
	}

	const Alert = () =>
		state.showAlert ? (
			<MaterialAlert severity='error' color='error'>
				{state.errors}
			</MaterialAlert>
		) : null;

	return (
		<Container component='main' maxWidth='xs'>
			<Box sx={styles.boxContainer}>
				<Avatar sx={styles.iconForm}>
					<NoteAdd />
				</Avatar>
				<Typography component='h1' variant='h5'>
					Create an account!
				</Typography>
				<Box
					component='form'
					noValidate
					onSubmit={handleSubmit}
					sx={styles.boxContainerForm}
				>
					<Grid container spacing={2}>
						<Grid item xs={12}>
							<Input
								placeholderText='Enter name'
								labelText='Name'
								required
								onChange={handleInputChange('name')}
							/>
						</Grid>
						<Grid item xs={12}>
							<TextField
								required
								fullWidth
								id='email'
								label='Email'
								name='email'
								autoComplete='email'
								onChange={handleInputChange('email')}
							/>
						</Grid>
						<Grid item xs={12}>
							<TextField
								required
								fullWidth
								name='password'
								label='Password'
								type='password'
								id='password'
								autoComplete='new-password'
								onChange={handleInputChange('password')}
							/>
						</Grid>
						<Grid item xs={12}>
							<Alert />
						</Grid>
					</Grid>
					<Button type='submit' variant='contained' sx={styles.logInButtom}>
						Registration
					</Button>
					<Grid container justifyContent='flex-end'>
						<Grid item>
							{loginMessage}
							<Link to='/login'>Log in</Link>
						</Grid>
					</Grid>
				</Box>
			</Box>
		</Container>
	);
}

export default Registration;
