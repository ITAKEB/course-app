import React, { useEffect, useState } from 'react';
import CourseForm from '../CourseForm/CourseForm';
import { authorValidator, courseValidator } from '../../helpers/validators';
import { asyncActionSaveCourse } from '../../store/courses/thunk';
import {
	asyncActionGetAllAuthors,
	asyncActionSaveAuthor,
} from '../../store/authors/thunk';
import { getUserSession } from '../../helpers/helpers';
import { mkPostCoursePayload } from '../../services/courses';
import { mkPostAuthorPayload } from '../../services/authors';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

function CreateCourse() {
	const dispatch = useDispatch();
	const authorsSelector = useSelector((state) => state.authors);

	const navigate = useNavigate();

	const authorsNameById = {};
	authorsSelector.forEach(
		(author) => (authorsNameById[author.id] = author.name)
	);
	const [courseInfo, setCourseInfo] = useState({
		title: '',
		description: '',
		currentAuthorsList: [],
		authorsSelected: [],
		authorsIds: [],
		duration: 0,
	});
	const [author, setAuthor] = useState({
		name: '',
	});

	useEffect(() => {
		//If authorsSelector is empty, so get all from bakend
		if (authorsSelector.length < 1) {
			dispatch(asyncActionGetAllAuthors());
			return;
		}

		const updateState = (prevSt) => {
			let currentAuthorsList = prevSt.currentAuthorsList;
			let authorsIds = prevSt.authorsIds;

			authorsSelector.forEach((fetchAuthor) => {
				if (
					!currentAuthorsList.find((author) => author.id === fetchAuthor.id) &&
					!authorsIds.includes(fetchAuthor.id)
				) {
					currentAuthorsList.push(fetchAuthor);
				}
			});

			return { ...prevSt, currentAuthorsList };
		};

		setCourseInfo(updateState);
	}, [authorsSelector, dispatch]);

	function onChangeCourseInfo(attr) {
		return (evt) => {
			evt.preventDefault();
			setCourseInfo((prevSt) => ({ ...prevSt, [attr]: evt.target.value }));
		};
	}

	function onChangeAuthorName(evt) {
		evt.preventDefault();
		setAuthor((prevSt) => ({ ...prevSt, name: evt.target.value }));
	}

	function handleAddAuthor(id, name) {
		return (evt) => {
			evt.preventDefault();
			let newAuthors = courseInfo.authorsSelected;
			let newAuthorsIds = courseInfo.authorsIds;

			let newcurrentAuthorsList = courseInfo.currentAuthorsList.filter(
				(author) => author.id !== id
			);
			setCourseInfo((prevSt) => ({
				...prevSt,
				currentAuthorsList: newcurrentAuthorsList,
			}));

			newAuthors.push({ name, id });
			newAuthorsIds.push(id);
			setCourseInfo((prevSt) => ({
				...prevSt,
				authorsSelected: newAuthors,
				authorsIds: newAuthorsIds,
			}));
		};
	}

	function handleDeleteAuthor(id) {
		return (evt) => {
			evt.preventDefault();
			let authorDeleted = null;
			let newAuthors = courseInfo.authorsSelected.filter((author) => {
				if (author.id !== id) {
					return true;
				} else {
					authorDeleted = author;
					return false;
				}
			});
			let newAuthorsId = courseInfo.authorsIds.filter(
				(authorId) => authorId !== id
			);

			setCourseInfo((prevSt) => ({
				...prevSt,
				authorsSelected: newAuthors,
				authorsIds: newAuthorsId,
			}));

			let newcurrentAuthorsList = courseInfo.currentAuthorsList;
			newcurrentAuthorsList.push(authorDeleted);
			setCourseInfo((prevSt) => ({
				...prevSt,
				currentAuthorsList: newcurrentAuthorsList,
			}));
		};
	}

	function onSubmitAuthor(evt) {
		evt.preventDefault();
		const validator = authorValidator(author);
		if (!validator.allValid) {
			window.alert('Please, add an author name');
			return;
		}
		const newAuthor = {
			name: author.name,
		};
		const session = getUserSession();
		const authorPayload = mkPostAuthorPayload(newAuthor);

		dispatch(asyncActionSaveAuthor(authorPayload, session.token));

		setAuthor((prevSt) => ({ ...prevSt, name: '' }));
	}

	function onSubmitCourse(evt) {
		evt.preventDefault();
		const validator = courseValidator(courseInfo);
		if (!validator.allValid) {
			window.alert('Please, fill in all fields');
			return;
		}

		const newCourse = {
			title: courseInfo.title,
			description: courseInfo.description,
			duration: courseInfo.duration,
			authors: courseInfo.authorsIds,
		};
		const session = getUserSession();
		const coursePayload = mkPostCoursePayload(newCourse);

		dispatch(asyncActionSaveCourse(coursePayload, session.token));

		navigate('/courses');
		console.log('Sucess Created');
	}

	return (
		<CourseForm
			onSubmitCourse={onSubmitCourse}
			onSubmitAuthor={onSubmitAuthor}
			handleDeleteAuthor={handleDeleteAuthor}
			onChangeAuthorName={onChangeAuthorName}
			handleAddAuthor={handleAddAuthor}
			onChangeCourseInfo={onChangeCourseInfo}
			courseInfo={courseInfo}
			author={author}
		/>
	);
}

export default CreateCourse;
