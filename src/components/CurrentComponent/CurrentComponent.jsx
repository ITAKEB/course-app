import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import Courses from '../Courses/Courses';
import CreateCourse from '../CreateCourse/CreateCourse';
import Login from '../Login/Login';
import Registration from '../Registration/Registration';
import Layout from '../Layout/Layout';
import CourseInfo from '../CourseInfo/CourseInfo';
import PrivateRouter from '../PrivateRouter/PrivateRouter';
import UpdateCourse from '../UpdateCourse/UpdateCourse';

const styles = {
	currentComponent_content: {
		width: '90vw',
		margin: 'auto',
		maxWidth: '1200px',
	},
};

function CurrentComponent() {
	return (
		<Router>
			<div style={styles.currentComponent_content}>
				<Routes>
					<Route path='/' element={<Layout />}>
						<Route
							path='/courses/add'
							element={
								<PrivateRouter>
									<CreateCourse />
								</PrivateRouter>
							}
						/>
						<Route
							path='/courses/update/:courseId'
							element={
								<PrivateRouter>
									<UpdateCourse />
								</PrivateRouter>
							}
						/>

						<Route path='/courses' element={<Courses />} />
						<Route path='/courses/:courseId/' element={<CourseInfo />} />
						<Route path='/login' element={<Login />} />
						<Route path='/registration' element={<Registration />} />
					</Route>
				</Routes>
			</div>
		</Router>
	);
}

export default CurrentComponent;
