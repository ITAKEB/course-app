import React, { useEffect, useState } from 'react';
import { Grid } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';

import CourseCard from './components/CourseCard/CourseCard';
import {
	toDate,
	toHoursAndMinutes,
	getAuthorsName,
	getUserSession,
} from '../../helpers/helpers';
import Button from '../../common/Button/Button';
import SearchBar from './components/SearchBar/SearchBar';
import Link from '../../common/Link/Link';
import {
	asyncActionDeleteCourse,
	asyncActionGetAllCourses,
} from '../../store/courses/thunk';
import { asyncActionGetAllAuthors } from '../../store/authors/thunk';

const styles = {
	course__actions: {
		marginTop: '30px',
		marginBottom: '10px',
		display: 'flex',
		direction: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
	},
};

function Courses() {
	const dispatch = useDispatch();
	const coursesSelector = useSelector((state) => state.courses);
	const userSelector = useSelector((state) => state.user);
	const authorsSelector = useSelector((state) => state.authors);

	const [state, setState] = useState({
		searchedInput: '',
		searchedCoursesList: null,
	});

	useEffect(() => {
		dispatch(asyncActionGetAllAuthors());
		dispatch(asyncActionGetAllCourses());
	}, [dispatch]);

	function handleSearchCourse(searchInputText) {
		if (searchInputText.length < 1) {
			setState((prevSt) => ({
				...prevSt,
				searchedCoursesList: null,
			}));
			return;
		}

		const searchInput = searchInputText.toLowerCase();

		let newSearchCoursesList = coursesSelector.filter(
			(course) =>
				course.title.toLowerCase().includes(searchInput) ||
				course.id.toLowerCase().includes(searchInput)
		);

		setState((prevSt) => ({
			...prevSt,
			searchedCoursesList: newSearchCoursesList,
		}));
	}

	function handleDeleteCourse(id) {
		const session = getUserSession();

		dispatch(asyncActionDeleteCourse(id, session.token));
		let newSearchCoursesList = coursesSelector.filter(
			(course) => course.id !== id
		);
		setState((prevSt) => ({
			...prevSt,
			searchedInput: '',
			searchedCoursesList: newSearchCoursesList,
		}));
	}

	const currentCourses = state.searchedCoursesList
		? state.searchedCoursesList
		: coursesSelector;

	const createNewCourseButton =
		userSelector.role === 'admin' ? (
			<Link to='/courses/add'>
				<Button buttonText='Create New Course' color='success'>
					Add new course
				</Button>
			</Link>
		) : null;

	return (
		<div>
			<div style={styles.course__actions}>
				<SearchBar handleSearchCourse={handleSearchCourse} />
				{createNewCourseButton}
			</div>
			<CurrentCourses
				coursesList={currentCourses}
				authorsList={authorsSelector}
				handleDeleteCourse={handleDeleteCourse}
				isAdmin={userSelector.role === 'admin'}
			/>
		</div>
	);
}

function CurrentCourses({
	coursesList,
	authorsList,
	handleDeleteCourse,
	isAdmin,
}) {
	const authorsNameById = {};
	authorsList.forEach((author) => (authorsNameById[author.id] = author.name));

	const courses = coursesList.map((course) => (
		<CourseCard
			key={course.id}
			courseId={course.id}
			courseTitle={course.title}
			courseDescription={course.description}
			createdAt={toDate(course.creationDate)}
			duration={toHoursAndMinutes(course.duration)}
			authors={getAuthorsName(course.authors, authorsNameById)}
			handleDeleteCourse={handleDeleteCourse}
			isAdmin={isAdmin}
		/>
	));

	return (
		<Grid
			data-testid='courses-list-container'
			id='courses-list-container'
			container
			gap={5}
		>
			{courses}
		</Grid>
	);
}

export default Courses;
