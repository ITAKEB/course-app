import React from 'react';
import '@testing-library/jest-dom';
import { Provider } from 'react-redux';
import { render, fireEvent } from '@testing-library/react';
import { BrowserRouter, MemoryRouter, Route, Routes } from 'react-router-dom';

import Courses from '../Courses';
import CreateCourse from '../../CreateCourse/CreateCourse';

test('Courses should display amount of CourseCard equal length of courses array', () => {
	const mockedState = {
		user: {
			isAuth: true,
			name: 'Test Name',
		},
		courses: [
			{
				id: 0,
				title: 'course title',
				description: 'course description',
				creationDate: '03-12-2015',
				authors: [],
				duration: 120,
			},
			{
				id: 1,
				title: 'course title 2',
				description: 'course description 2',
				creationDate: '03-12-2015',
				authors: [],
				duration: 120,
			},
		],
		authors: [],
	};
	const mockedStore = {
		getState: () => mockedState,
		subscribe: jest.fn(),
		dispatch: jest.fn(),
	};

	const component = render(
		<Provider store={mockedStore}>
			<BrowserRouter>
				<Courses />
			</BrowserRouter>
		</Provider>
	);

	expect(component.getByTestId('courses-list-container')).toBeInTheDocument();
	const coursesContainer = component.getByTestId('courses-list-container');

	expect(coursesContainer.children.length).toEqual(mockedState.courses.length);
});

test('Courses should display Empty container if courses array lenght is 0', () => {
	const mockedState = {
		user: {
			isAuth: true,
			name: 'Test Name',
		},
		courses: [],
		authors: [],
	};
	const mockedStore = {
		getState: () => mockedState,
		subscribe: jest.fn(),
		dispatch: jest.fn(),
	};

	const component = render(
		<Provider store={mockedStore}>
			<BrowserRouter>
				<Courses />
			</BrowserRouter>
		</Provider>
	);

	expect(component.getByTestId('courses-list-container')).toBeInTheDocument();

	const coursesContainer = component.getByTestId('courses-list-container');
	expect(coursesContainer.children.length).toEqual(0);
});

test('CourseForm should display after cliking on a button "Add new course"', () => {
	const mockedState = {
		user: {
			isAuth: true,
			name: 'Test Name',
			role: 'admin',
		},
		courses: [],
		authors: [],
	};
	const mockedStore = {
		getState: () => mockedState,
		subscribe: jest.fn(),
		dispatch: jest.fn(),
	};

	//First way - BrowserRouter
	/* 
		const component = render(
			<Provider store={mockedStore}>
				<BrowserRouter>
					<Routes>
						<Route path='/' element={<Courses />} />
						<Route path='/courses/add' element={<CreateCourse />} />
					</Routes>
				</BrowserRouter>
			</Provider>
		);
	*/

	//Second way - MemoryRouter
	const component = render(
		<Provider store={mockedStore}>
			<MemoryRouter initialEntries={['/courses']}>
				<Routes>
					<Route path='/courses' element={<Courses />} />
					<Route path='/courses/add' element={<CreateCourse />} />
				</Routes>
			</MemoryRouter>
		</Provider>
	);

	expect(component.getByText('Add new course')).toBeInTheDocument();
	const addNewCourseLink = component.getByText('Add new course');

	fireEvent.click(addNewCourseLink);
	expect(component.getByTestId('course-form-container')).toBeInTheDocument();
});
