import React, { useState } from 'react';
import Input from '../../../../common/Input/Input';
import Button from '../../../../common/Button/Button';

const styles = {
	searchBar_form: {
		display: 'flex',
		direction: 'row',
		alignItems: 'center',
		gap: '10px',
	},
};

function SearchBar({ handleSearchCourse }) {
	const [state, setState] = useState({
		searchInputText: '',
	});

	function onChange(evt) {
		evt.preventDefault();
		setState((prevState) => ({
			...prevState,
			searchInputText: evt.target.value,
		}));
	}

	function handleOnSubmit(evt) {
		evt.preventDefault();
		handleSearchCourse(state.searchInputText);
	}

	return (
		<div>
			<form style={styles.searchBar_form} onSubmit={handleOnSubmit}>
				<Input
					placeholderText='Enter course name...'
					labelText='Search Course'
					onChange={onChange}
				/>
				<Button type='submit'>Search</Button>
			</form>
		</div>
	);
}

export default SearchBar;
