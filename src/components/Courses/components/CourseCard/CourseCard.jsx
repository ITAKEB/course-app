import React from 'react';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import {
	Grid,
	Card,
	Typography,
	Button as MaterialButton,
} from '@mui/material';

import Button from '../../../../common/Button/Button';
import Link from '../../../../common/Link/Link';

const styles = {
	courseCard: {
		padding: '20px',
		width: '100%',
	},
	courseCard_authors: {
		textOverflow: 'ellipsis',
	},
};

function CourseCard({
	courseTitle,
	courseId,
	courseDescription,
	authors,
	duration,
	createdAt,
	handleDeleteCourse,
	isAdmin,
}) {
	const buttonText = 'Show Course';

	const onDelete = (evt) => {
		evt.preventDefault();
		handleDeleteCourse(courseId);
	};

	const adminButtons = isAdmin ? (
		<>
			<Grid item>
				<Link to={`/courses/update/${courseId}`}>
					<MaterialButton variant='outlined'>
						<EditIcon />
					</MaterialButton>
				</Link>
			</Grid>
			<Grid item>
				<MaterialButton variant='outlined' onClick={onDelete}>
					<DeleteIcon />
				</MaterialButton>
			</Grid>
		</>
	) : null;

	return (
		<Card sx={styles.courseCard}>
			<Grid item container direction='row' spacing={1}>
				<Grid item md={8}>
					<h1>{courseTitle}</h1>
					<p>{courseDescription}</p>
				</Grid>
				<Grid
					item
					container
					direction='column'
					alignItems='center'
					justifyContent='center'
					spacing={4}
					md={4}
				>
					<Grid item container justifyContent='flex-start' spacing={2}>
						<Grid item xs={12}>
							<Typography noWrap sx={styles.courseCard_authors}>
								<b>Authors:</b> {authors.toString()}
							</Typography>
						</Grid>
						<Grid item xs={12}>
							<span>
								<b>Duration:</b> {duration} hours
							</span>
						</Grid>
						<Grid item xs={12}>
							<span>
								<b>Created:</b> {createdAt}
							</span>
						</Grid>
					</Grid>
					<Grid id='actionButtons' item container direction='row' spacing={2}>
						<Grid item>
							<Link to={`${courseId}`}>
								<Button>{buttonText}</Button>
							</Link>
						</Grid>
						{adminButtons}
					</Grid>
				</Grid>
			</Grid>
			<br />
		</Card>
	);
}

export default CourseCard;
