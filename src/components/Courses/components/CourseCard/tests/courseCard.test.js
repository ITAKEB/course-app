import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import '@testing-library/jest-dom';
import { render } from '@testing-library/react';
// NOTE: jest-dom adds handy assertions to Jest and is recommended, but not required

import CourseCard from '../CourseCard';
import { toDate, toHoursAndMinutes } from '../../../../../helpers/helpers';

test('CourseCard should display title', () => {
	const courseInfo = {
		courseTitle: 'Course Title',
		courseId: 'course',
		authors: [],
		courseDescription: 'Course Description',
		createdAt: '2015-12-02',
		handleDeleteCourse: () => {},
		isAdmin: false,
	};

	const component = render(
		<BrowserRouter>
			<CourseCard {...courseInfo} />
		</BrowserRouter>
	);

	expect(component.queryByText('Course Title')).toBeInTheDocument();
});

test('CourseCard should display description', () => {
	const mockedCourseInfo = {
		courseTitle: 'Course Title',
		courseId: 'course',
		authors: [],
		courseDescription: 'Course Description',
		createdAt: '2015-12-02',
		handleDeleteCourse: () => {},
		isAdmin: false,
	};

	const component = render(
		<BrowserRouter>
			<CourseCard {...mockedCourseInfo} />
		</BrowserRouter>
	);

	expect(
		component.queryByText(mockedCourseInfo.courseTitle)
	).toBeInTheDocument();
});

test('CourseCard should display duration in the correct format', () => {
	const mockedCourseInfo = {
		courseTitle: 'Course Title',
		courseId: 'course',
		authors: [],
		courseDescription: 'Course Description',
		createdAt: '2015-12-02',
		handleDeleteCourse: () => {},
		isAdmin: false,
		duration: toHoursAndMinutes(120),
	};
	const correctDurationFormat = `Duration: ${mockedCourseInfo.duration} hours`;

	const component = render(
		<BrowserRouter>
			<CourseCard {...mockedCourseInfo} />
		</BrowserRouter>
	);

	expect(component.queryByText('Duration:')).toBeInTheDocument();
	const durationContainer = component.queryByText('Duration:').parentElement;

	expect(durationContainer.textContent).toEqual(correctDurationFormat);
});

test('CourseCard should display authors list', () => {
	const mockedCourseInfo = {
		courseTitle: 'Course Title',
		courseId: 'course',
		authors: ['John', 'Wick'],
		courseDescription: 'Course Description',
		createdAt: '2015-12-02',
		handleDeleteCourse: () => {},
		isAdmin: false,
		duration: toHoursAndMinutes(120),
	};

	const component = render(
		<BrowserRouter>
			<CourseCard {...mockedCourseInfo} />
		</BrowserRouter>
	);

	expect(component.queryByText('Authors:')).toBeInTheDocument();
	const authorsContainer = component.queryByText('Authors:').parentElement;

	expect(authorsContainer.textContent).toContain(mockedCourseInfo.authors[0]);
	expect(authorsContainer.textContent).toContain(mockedCourseInfo.authors[1]);
});

test('CourseCard should display created date in the correct format', () => {
	const mockedCourseInfo = {
		courseTitle: 'Course Title',
		courseId: 'course',
		authors: ['John', 'Wick'],
		courseDescription: 'Course Description',
		createdAt: toDate('03-05-2023'),
		handleDeleteCourse: () => {},
		isAdmin: false,
		duration: toHoursAndMinutes(120),
	};
	const correctCreatedFormat = `Created: ${mockedCourseInfo.createdAt}`;

	const component = render(
		<BrowserRouter>
			<CourseCard {...mockedCourseInfo} />
		</BrowserRouter>
	);

	expect(component.queryByText('Created:')).toBeInTheDocument();
	const createdContainer = component.queryByText('Created:').parentElement;

	expect(createdContainer.textContent).toEqual(correctCreatedFormat);
});
