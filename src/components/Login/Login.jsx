import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { AccountCircle } from '@mui/icons-material';
import {
	Alert as MaterialAlert,
	Avatar,
	Box,
	Container,
	Grid,
	TextField,
	Typography,
} from '@mui/material';

import Button from '../../common/Button/Button';
import { Link, useNavigate } from 'react-router-dom';
import { mkPostUserPayload, postUser } from '../../services/user';
import { asyncActionSaveSession } from '../../store/user/thunk';

const styles = {
	boxContainer: {
		marginTop: 8,
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
	},
	iconForm: {
		m: 1,
		bgcolor: '#FF6161',
	},
	boxContainerForm: {
		mt: 3,
	},
	logInButtom: {
		mt: 3,
		mb: 2,
		'&.MuiButtonBase-root': {
			marginTop: '25px',
		},
		width: '100%',
	},
};

function Login() {
	const dispatch = useDispatch();
	const navigate = useNavigate();

	const [state, setState] = useState({
		email: '',
		password: '',
		errors: null,
		showAlert: false,
	});

	const registrationMessage = 'If you dont have an account you can ';

	function handleInputChange(attr) {
		return (evt) => {
			evt.preventDefault();
			const value = evt.target.value;
			setState((prevSt) => ({ ...prevSt, [attr]: value }));
		};
	}

	async function handleSubmit(evt) {
		evt.preventDefault();
		const user = mkPostUserPayload(state.email, state.password);
		const response = await postUser(user);

		if (response.isSuccess) {
			const jwt = response.result;

			dispatch(asyncActionSaveSession(jwt));

			navigate('/courses');
		} else {
			const errors = response.errors
				? response.errors
				: 'An error has occurred';

			setState((prevSt) => ({ ...prevSt, errors, showAlert: true }));
			console.log(errors);
		}
	}

	const Alert = () =>
		state.showAlert ? (
			<MaterialAlert severity='error' color='error'>
				{state.errors}
			</MaterialAlert>
		) : null;

	return (
		<Container component='main' maxWidth='xs'>
			<Box sx={styles.boxContainer}>
				<Avatar sx={styles.iconForm}>
					<AccountCircle />
				</Avatar>
				<Typography component='h1' variant='h5'>
					Welcome to Online Courses!
				</Typography>
				<Box
					component='form'
					noValidate
					onSubmit={handleSubmit}
					sx={styles.boxContainerForm}
				>
					<Grid container spacing={2}>
						<Grid item xs={12}>
							<TextField
								required
								fullWidth
								id='email'
								label='Email'
								name='email'
								autoComplete='email'
								onChange={handleInputChange('email')}
							/>
						</Grid>
						<Grid item xs={12}>
							<TextField
								required
								fullWidth
								name='password'
								label='Password'
								type='password'
								id='password'
								autoComplete='new-password'
								onChange={handleInputChange('password')}
							/>
						</Grid>
						<Grid item xs={12}>
							<Alert />
						</Grid>
					</Grid>
					<Button type='submit' sx={styles.logInButtom}>
						Log in
					</Button>
					<Grid container justifyContent='flex-end'>
						<Grid item>
							{registrationMessage}
							<Link to='/registration'>Registration</Link>
						</Grid>
					</Grid>
				</Box>
			</Box>
		</Container>
	);
}

export default Login;
