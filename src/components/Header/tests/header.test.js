import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import '@testing-library/jest-dom';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';

import Header from '../Header';

test("Header should have logo and user's name", () => {
	const mockedState = {
		user: {
			isAuth: true,
			name: 'Test Name',
		},
		courses: [],
		authors: [],
	};
	const mockedStore = {
		getState: () => mockedState,
		subscribe: jest.fn(),
		dispatch: jest.fn(),
	};

	const component = render(
		<Provider store={mockedStore}>
			<BrowserRouter>
				<Header />
			</BrowserRouter>
		</Provider>
	);

	expect(component.queryByText(mockedState.user.name)).toBeInTheDocument();
	expect(component.getByAltText('logo')).toBeInTheDocument();

	const logoImg = component.getByAltText('logo');
	expect(logoImg.src).toContain('logo.png');
});
