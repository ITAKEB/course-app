import React from 'react';

const styles = {
	imgLogo: {
		width: '150px',
		height: 'auto',
	},
};

function Logo({ imgUrl }) {
	return <img style={styles.imgLogo} src={imgUrl} alt='logo' />;
}

export default Logo;
