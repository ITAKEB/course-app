import React from 'react';
import AppBar from '@mui/material/AppBar';
import { Container, Typography } from '@mui/material';
import { useSelector } from 'react-redux';

import Logo from './Logo/Logo';
import logoImg from './Logo/Assets/logo.png';
import Button from '../../common/Button/Button';
import Link from '../../common/Link/Link';

const styles = {
	appBar: {
		'&.MuiAppBar-root': {
			backgroundColor: '#FFFFFF',
			boxShadow: 'none',
		},
		marginBottom: '20px',
	},
	appBar_userName: {
		fontWeight: 'bold',
		color: '#000000',
	},
	appBar_content: {
		display: 'flex',
		justifyContent: 'space-between',
	},
	appBar_content_actions: {
		display: 'flex',
		flexDirection: 'row',
		alignItems: 'center',
		gap: '20px',
	},
};

function Header({ handlerLogOut }) {
	const userSelector = useSelector((state) => state.user);

	const currentAction = userSelector.token ? (
		<Link to='/login'>
			<Button color='error' onClick={handlerLogOut} variant='outlined'>
				Log out
			</Button>
		</Link>
	) : (
		<Link to='/login'>
			<Button color='primary' variant='outlined'>
				Log in
			</Button>
		</Link>
	);

	return (
		<div>
			<AppBar sx={styles.appBar} position='static'>
				<Container sx={styles.appBar_content}>
					<Logo imgUrl={logoImg} />
					<div style={styles.appBar_content_actions}>
						<Typography
							variant='h6'
							component='div'
							sx={styles.appBar_userName}
						>
							{userSelector.name}
						</Typography>
						{currentAction}
					</div>
				</Container>
			</AppBar>
		</div>
	);
}

export default Header;
