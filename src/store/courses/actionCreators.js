import { coursesActionTypes } from './actionTypes';

export function actionSaveCourse(course) {
	return {
		type: coursesActionTypes.save,
		payload: {
			course,
		},
	};
}

export function actionDeleteCourse(id) {
	return {
		type: coursesActionTypes.delete,
		payload: {
			id,
		},
	};
}

export function actionUpdateCourse(id, courseUpdated) {
	return {
		type: coursesActionTypes.update,
		payload: {
			id,
			courseUpdated: {
				id,
				...courseUpdated,
			},
		},
	};
}

export function actionGetAllCourses(courses) {
	return {
		type: coursesActionTypes.getAll,
		payload: {
			courses,
		},
	};
}
