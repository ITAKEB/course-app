import { coursesActionTypes } from './actionTypes';

const coursesInitialState = []; // default value - empty array. After success getting courses from API - array of courses.

function coursesReducer(state = coursesInitialState, action) {
	switch (action.type) {
		case coursesActionTypes.save: {
			const { course } = action.payload;
			const result = state.find((stCourse) => stCourse.id === course.id);

			if (result) {
				return state;
			}

			return state.concat(course);
		}
		case coursesActionTypes.delete: {
			const { id } = action.payload;

			return state.filter((course) => course.id !== id);
		}

		case coursesActionTypes.update: {
			const { id, courseUpdated } = action.payload;

			//Im not sure if this is completely innmutable
			const newState = state.filter((course) => course.id !== id);
			return [...newState, courseUpdated];
		}

		case coursesActionTypes.getAll: {
			const { courses } = action.payload;

			return [...courses];
		}

		default:
			return state;
	}
}

export default coursesReducer;
