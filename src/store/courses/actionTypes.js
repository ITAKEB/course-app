export const coursesActionTypes = {
	save: 'courses/save',
	delete: 'courses/delete',
	update: 'courses/update',
	getAll: 'courses/getAll',
};
