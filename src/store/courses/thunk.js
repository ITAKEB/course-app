import {
	deleteCourseById,
	getAllCourses,
	postCourse,
	updateCourseById,
} from '../../services/courses';
import {
	actionDeleteCourse,
	actionGetAllCourses,
	actionSaveCourse,
	actionUpdateCourse,
} from './actionCreators';

export const asyncActionGetAllCourses = () => {
	return async (dispatch) => {
		const courses = await getAllCourses();
		if (!courses.isSuccess) {
			return;
		}

		return dispatch(actionGetAllCourses(courses.result));
	};
};

export const asyncActionSaveCourse = (coursePayload, jwt) => {
	return async (dispatch) => {
		const course = await postCourse(coursePayload, jwt);
		if (!course.isSuccess) {
			return;
		}

		return dispatch(actionSaveCourse(course.result));
	};
};

export const asyncActionUpdateCourse = (coursePayload, id, jwt) => {
	return async (dispatch) => {
		const course = await updateCourseById(coursePayload, jwt, id);
		if (!course.isSuccess) {
			return;
		}

		return dispatch(actionUpdateCourse(id, course.result));
	};
};

export const asyncActionDeleteCourse = (id, jwt) => {
	return async (dispatch) => {
		const response = await deleteCourseById(jwt, id);

		if (!response.isSuccess) {
			return;
		}

		return dispatch(actionDeleteCourse(id));
	};
};
