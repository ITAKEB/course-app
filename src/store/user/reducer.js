import { actionUserTypes } from './actionTypes';

const userInitialState = {
	isAuth: false,
	name: '',
	email: '',
	token: '',
	role: '',
};

function userReducer(state = userInitialState, action) {
	switch (action.type) {
		case actionUserTypes.save:
			const { isAuth, name, email, token, role } = action.payload;
			return {
				...state,
				isAuth: isAuth,
				name: name,
				email: email,
				token: token,
				role: role,
			};
		case actionUserTypes.delete:
			return {
				...state,
				isAuth: false,
				name: '',
				email: '',
				token: '',
				role: '',
			};
		default:
			return state;
	}
}

export default userReducer;
