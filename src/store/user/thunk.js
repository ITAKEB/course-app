import { clearUserSession, saveUserSession } from '../../helpers/helpers';
import { getUser, logoutUser } from '../../services/user';
import { actionDeleteUser, actionSaveUser } from './actionCreators';

export const asyncActionSaveSession = (jwt) => {
	return async (dispatch) => {
		const user = await getUser(jwt);
		if (!user.isSuccess) {
			return;
		}

		saveUserSession(jwt, user.result);

		return dispatch(actionSaveUser(user.result, jwt));
	};
};

export const asyncActionDeleteSession = (jwt) => {
	return async (dispatch) => {
		const response = await logoutUser(jwt);
		if (!response.isSuccess) {
			console.log(response.errors);
		}
		clearUserSession();

		return dispatch(actionDeleteUser());
	};
};
