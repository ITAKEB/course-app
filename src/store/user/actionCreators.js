import { actionUserTypes } from './actionTypes';

export function actionSaveUser(user, token) {
	return {
		type: actionUserTypes.save,
		payload: {
			isAuth: true,
			name: user.name,
			email: user.email,
			token: token,
			role: user.role,
		},
	};
}

export function actionDeleteUser() {
	return {
		type: actionUserTypes.delete,
	};
}
