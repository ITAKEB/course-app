import { authorsActionTypes } from './actionTypes';

export function actionSaveAuthor(author) {
	return {
		type: authorsActionTypes.save,
		payload: {
			author,
		},
	};
}

export function actionGetAllAuthors(authors) {
	return {
		type: authorsActionTypes.getAll,
		payload: {
			authors,
		},
	};
}
