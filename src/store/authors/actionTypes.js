export const authorsActionTypes = {
	save: 'authors/save',
	delete: 'authors/delete',
	getAll: 'authors/getAll',
};
