import { authorsActionTypes } from './actionTypes';
const authorsInitialState = []; // default value - empty array. After success getting authors from API - array of authors.

function authorsReducer(state = authorsInitialState, action) {
	switch (action.type) {
		case authorsActionTypes.save:
			const { author } = action.payload;
			const result = state.find((stAuthor) => stAuthor.id === author.id);

			if (result) {
				return state;
			}

			return state.concat(author);

		case authorsActionTypes.delete:
			// const { id } = action.payload;
			return [...state];

		case authorsActionTypes.getAll:
			const { authors } = action.payload;

			return [...authors];

		default:
			return state;
	}
}

export default authorsReducer;
