import { getAllAuthors, postAuthor } from '../../services/authors';
import { actionGetAllAuthors, actionSaveAuthor } from './actionsCreators';

export const asyncActionGetAllAuthors = () => {
	return async (dispatch) => {
		const authors = await getAllAuthors();
		if (!authors.isSuccess) {
			return;
		}

		return dispatch(actionGetAllAuthors(authors.result));
	};
};

export const asyncActionSaveAuthor = (authorPayload, jwt) => {
	return async (dispatch) => {
		const author = await postAuthor(authorPayload, jwt);
		if (!author.isSuccess) {
			return;
		}

		return dispatch(actionSaveAuthor(author.result));
	};
};
