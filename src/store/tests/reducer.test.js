import '@testing-library/jest-dom';
import coursesReducer from '../courses/reducer';
import {
	actionGetAllCourses,
	actionSaveCourse,
} from '../courses/actionCreators';

describe('coursesReducer tests', () => {
	test('Courses reducer should return the initial state', () => {
		expect(
			coursesReducer(undefined, {
				type: 'default',
			})
			//The default/intial state is an empty array
		).toEqual([]);
	});

	test('Courses reducer should handle SAVE_COURSE and return new state', () => {
		const newCourse = {
			id: 0,
			title: 'course title',
			description: 'course description',
			creationDate: '03-12-2015',
			authors: [],
			duration: 120,
		};

		//New state can't be an empty array, it should be an array with the new course inside
		expect(coursesReducer(undefined, actionSaveCourse(newCourse))).not.toEqual(
			[]
		);
	});

	test('Courses reducer should handle GET_COURSES and return new state', () => {
		const courses = [
			{
				id: 0,
				title: 'course title',
				description: 'course description',
				creationDate: '03-12-2015',
				authors: [],
				duration: 120,
			},
			{
				id: 1,
				title: 'course title 2',
				description: 'course description 2',
				creationDate: '03-12-2015',
				authors: [],
				duration: 120,
			},
		];

		//The state can't be an empty array it should be a new state with the new courses
		expect(coursesReducer(undefined, actionGetAllCourses(courses))).not.toEqual(
			[]
		);
	});
});
